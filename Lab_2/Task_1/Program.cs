﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Task_1
{
    static class Rand
    {
        public const int K = 20000;
        public static int a = 16807;
        public static int c = 0;
        public static double m = Math.Pow(2, 31) - 1;
        public static int min = 0;
        public static int max = 200;


        public static void LineRand(double x, int[] arr, int k = 0)
        {

            if (k < K)
            {
                x = (a * x + c) % m;
                k++;
                LineRand(x, arr, k);
                arr[k - 1] = (int)(min + (x % (max + 1 - min)));
            }
            else
                return;
        }

        public static void Print(int[] arr, long amount = K)

        {
            for (int i = 0; i < amount; i++)
                WriteLine($"{i + 1} - {arr[i]} ");
        }

        public static void Frequency(int[] arr, int[] freq)
        {
            int k = 0;
            double P;

            for (int i = 0; i <= max; i++)
            {
                k = 0;
                for (int j = 0; j < K; j++)
                    if (i == arr[j])
                        k++;

                freq[i] = k;
                P = 1.0 * freq[i] / K;
                WriteLine($"Число {i} зустрiчається {freq[i]} разiв, ймовiрнiсть  = {P}");

            }
        }

        public static double[] StatisticProbability(int[] arr, int[] freq)
        {
            double[] P = new double[K];
            int k = 0;

            for (int i = 0; i <= max; i++)
            {
                k = 0;
                for (int j = 0; j < K; j++)
                    if (i == arr[j])
                        k++;
                freq[i] = k;
                P[i] = 1.0 * freq[i] / K;
                WriteLine($"статична ймовiрнiсть числа {i}  =  {P[i]}");
            }
            return P;
        }

        public static double Expectation(int[] arr, double[] P)
        {
            double M = 0;
            for (int i = 0; i < K; i++)
                M += (double)(arr[i] * P[i]);
            return M;
        }

        public static double Dispersion(int[] arr, double M, double[] P)
        {
            double D = 0;
            for (int i = 0; i < K; i++)
                D += Math.Pow((arr[i] - M), 2) * P[i];

            return D;
        }

        public static double Deviation(double D)
        {
            return Math.Sqrt(D);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            OutputEncoding = Encoding.Unicode;
            double x = 1;
            int[] arr = new int[Rand.K];

            Rand.LineRand(x, arr);
            Rand.Print(arr, 200);


            int[] frequency = new int[Rand.K];
            Rand.Frequency(arr, frequency);


            double[] P = new double[Rand.K];
            P = Rand.StatisticProbability(arr, frequency);


            double M = Rand.Expectation(arr, P);
            WriteLine($"Математичне сподiвання = {M}");


            double D = Rand.Dispersion(arr, M, P);
            WriteLine($"Дисперсiя = {D}");


            double G = Rand.Deviation(D);
            WriteLine($"Середньоквадратичне вiдхилення = {G}");
        }
    }

}
