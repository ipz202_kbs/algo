﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Graph
    {
        public Graph()
        {
            Adj = new Dictionary<int, HashSet<int>>();
        }

        public Dictionary<int, HashSet<int>> Adj { get; private set; }

        public void AddEdge(int source, int target, int distance = 0)
        {
            if (Adj.ContainsKey(source))
            {
                try
                {
                    Adj[source].Add(target);
                }
                catch
                {
                    Console.WriteLine("Цей зв'язок уже є: " + source + " до " + target);
                }
            }
            else
            {
                var hs = new HashSet<int>();
                hs.Add(target);
                Adj.Add(source, hs);
            }
        }

        public void BFSWalkWithStartNode(int vertex)
        {
            var visited = new HashSet<int>();
            // Позначаємо цей вузол як відвіданий 
            visited.Add(vertex);
            // черга для BFS
            var q = new Queue<int>();
            // додаємо вузол до черги
            q.Enqueue(vertex);

            while (q.Count > 0)
            {
                var current = q.Dequeue();
                Console.Write(current + "->");

                if (Adj.ContainsKey(current))
                {
                    // проходимо через невідвідані вузли
                    foreach (int neighbour in Adj[current].Where(a => !visited.Contains(a)))
                    {
                        visited.Add(neighbour);
                        q.Enqueue(neighbour);
                    }
                }
            }
        }

        public void DFSWalkWithStartNode(int vertex)
        {
            var visited = new HashSet<int>();
            // Позначаємо цей вузол як відвіданий 
            visited.Add(vertex);
            // стек для DFS
            var s = new Stack<int>();
            // додаємо вузол в стек
            s.Push(vertex);

            while (s.Count > 0)
            {
                var current = s.Pop();
                Console.Write(current + "->");
                // позначаємо як відвіданий
                if (!visited.Contains(current))
                    visited.Add(current);

                if (Adj.ContainsKey(current))
                {
                    // проходимо через невідвідані вузли
                    foreach (int neighbour in Adj[current].Where(a => !visited.Contains(a)))
                    {
                        visited.Add(neighbour);
                        s.Push(neighbour);
                    }
                }
            }
        }

        private void Traverse(int v, HashSet<int> visited)
        {
            // Позначаємо цей вузол як відвіданий 
            visited.Add(v);
            Console.WriteLine(v);

            if (Adj.ContainsKey(v))
            {
                // проходимо через невідвідані вузли
                foreach (int neighbour in Adj[v].Where(a => !visited.Contains(a)))
                    Traverse(neighbour, visited);
            }
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            var graph = new Graph();

            Console.WriteLine("Нумерацiя мiст:");
            Console.WriteLine("0-Київ;  1-Житомир;  2-Бердичiв;  3-Вiнниця;  4-Тернопiль;  5-Хмельницький;   6-Новоград-Волинський;  7-Рiвне;  8-Луцьк;  9-Шепетiвка;  10-Бiла Церква;	11-Полтава;  12-Харкiв;  13-Умань; 14-Черкаси;  15-Кременчуг;  16-Прилуки;  17-Суми;    18-Миргород.");
            Console.WriteLine("\n");

            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(0, 3);

            graph.AddEdge(1, 4);
            graph.AddEdge(1, 5);
            graph.AddEdge(1, 6);

            graph.AddEdge(2, 7);
            graph.AddEdge(2, 8);
            graph.AddEdge(2, 9);

            graph.AddEdge(3, 10);
            graph.AddEdge(3, 11);

            graph.AddEdge(4, 12);
            graph.AddEdge(5, 13);
            graph.AddEdge(7, 14);
            graph.AddEdge(9, 15);

            graph.AddEdge(12, 16);
            graph.AddEdge(13, 17);

            graph.AddEdge(16, 18);

            Console.WriteLine("BFS:");
            graph.BFSWalkWithStartNode(0);
            Console.WriteLine("\n");

            Console.WriteLine("DFS:");
            graph.DFSWalkWithStartNode(0);
            Console.WriteLine("\n");
        }
    }

}
