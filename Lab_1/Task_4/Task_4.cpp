﻿#include <iostream>

using namespace std;

union numBit
{
    float a;
    struct num
    {
        unsigned short x0 : 1;
        unsigned short x1 : 1;
        unsigned short x2 : 1;
        unsigned short x3 : 1;
        unsigned short x4 : 1;
        unsigned short x5 : 1;
        unsigned short x6 : 1;
        unsigned short x7 : 1;
        unsigned short x8 : 1;
        unsigned short x9 : 1;
        unsigned short x10 : 1;
        unsigned short x11 : 1;
        unsigned short x12 : 1;
        unsigned short x13 : 1;
        unsigned short x14 : 1;
        unsigned short x15 : 1;
        unsigned short x16 : 1;
        unsigned short x17 : 1;
        unsigned short x18 : 1;
        unsigned short x19 : 1;
        unsigned short x20 : 1;
        unsigned short x21 : 1;
        unsigned short x22 : 1;
        unsigned short x23 : 1;
        unsigned short x24 : 1;
        unsigned short x25 : 1;
        unsigned short x26 : 1;
        unsigned short x27 : 1;
        unsigned short x28 : 1;
        unsigned short x29 : 1;
        unsigned short x30 : 1;
        unsigned short x31 : 1;
    }x;
}z;

union numByte
{
    float b;
    struct num1
    {
        unsigned short x0 : 8;
        unsigned short x1 : 8;
        unsigned short x2 : 8;
        unsigned short x3 : 8;
    }x;
}z1;

void main()
{

    float x;
    cout << "Enter the number: ";  cin >> x;
    cout << "The value is bitwise" << endl;
    z.a = x;
    cout << x << " - " << z.x.x31 << z.x.x30 << z.x.x29 << z.x.x28 << " " << z.x.x27 << z.x.x26 << z.x.x25 << z.x.x24 << " " << z.x.x23 << z.x.x22 << z.x.x21 << z.x.x20 << " " << z.x.x19 << z.x.x18 << z.x.x17 << z.x.x16 << " " << z.x.x15 << z.x.x14 << z.x.x13 << z.x.x12 << " " << z.x.x11 << z.x.x10 << z.x.x9 << z.x.x8 << " " << z.x.x7 << z.x.x6 << z.x.x5 << z.x.x4 << " " << z.x.x3 << z.x.x2 << z.x.x1 << z.x.x0 << endl;
    cout << "The value is byte byte" << endl;
    z1.b = x;
    cout << x << " - " << z1.x.x3 << " " << z1.x.x2 << " " << z1.x.x1 << " " << z1.x.x0 << endl;
    cout << "Sign - " << z.x.x31 << endl;
    cout << "Characteristic - " << z.x.x30 << z.x.x29 << z.x.x28 << z.x.x27 << " " << z.x.x26 << z.x.x25 << z.x.x24 << z.x.x23 << endl;
    cout << "Mantissa - " << z.x.x22 << z.x.x21 << z.x.x20 << z.x.x19 << " " << z.x.x18 << z.x.x17 << z.x.x16 << z.x.x15 << " " << z.x.x14 << z.x.x13 << z.x.x12 << z.x.x11 << "" << z.x.x10 << z.x.x9 << z.x.x8 << z.x.x7 << " " << z.x.x6 << z.x.x5 << z.x.x4 << z.x.x3 << " " << z.x.x2 << z.x.x1 << z.x.x0 << endl;
    cout << "The amount of memory of the variable x in the main function - " << sizeof(x) << endl << "The amount of memory of the union numBit - " << sizeof(numBit) << " numByte - " << sizeof(numByte) << endl;
}
