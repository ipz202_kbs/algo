﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Task_1
{
    class Program
    {
        public class HeapSort

        {
            public void sort(int[] arr)
            {
                int n = arr.Length;

                // Побудова кучі (перегрупування масиву)
                for (int i = n / 2 - 1; i >= 0; i--)
                    heapify(arr, n, i);

                // По черзі витягуєм усі елементи кучі
                for (int i = n - 1; i >= 0; i--)
                {
                    // Переміщуємо поточний корінь на кінець
                    int temp = arr[0];
                    arr[0] = arr[i];
                    arr[i] = temp;

                    // викликаємо процедуру heapify на вже зменшенній кучі
                    heapify(arr, i, 0);
                }
            }

            //Процедура для перетворення в двійкову кучу піддерева з корневим вузлом, який являє
            // собою індекс в масиві

            void heapify(int[] arr, int n, int i)
            {
                int largest = i;
                // Ініціалізуємо невеликий елемент як корінь
                int left = 2 * i + 1;
                int right = 2 * i + 2;

                // Якщо лівий дочірній елемент більше заданого кореня
                if (left < n && arr[left] > arr[largest])
                    largest = left;

                // Якщо правий дочірній елемент більше за найбільший на данний момент елемент
                if (right < n && arr[right] > arr[largest])
                    largest = right;

                // Якщо найбільший елемент не є коренем
                if (largest != i)
                {
                    int swap = arr[i];
                    arr[i] = arr[largest];
                    arr[largest] = swap;

                    //За допомогою рекурсії перетворюємо у двійкову кучу вищенаведене піддере-во
                    heapify(arr, n, largest);
                }
            }
        }
        public class ShellSort
        {
            public void sort(float[] array)
            {
                int i, j, step = 1;
                float tmp;

                //Відбувається сортування включеннями з відстаннями, що зменшуються
                for (step = 1; step > 0;)
                {
                    for (i = step; i < array.Length; i++)
                    {


                        tmp = array[i];
                        for (j = i; j >= step; j -= step)
                        {
                            if (tmp < array[j - step])
                                array[j] = array[j - step];
                            else
                                break;
                        }
                        array[j] = tmp;
                    }
                    if (array.Length % 2 == 0)
                        step = 9 * 2 ^ step - 9 * 2 ^ (step / 2) + 1;
                    else
                        step = 8 * 2 ^ step - 6 * 2 ^ ((step + 1) / 2) + 1;

                }
            }
        }

        public class CountingSort
        {
            public void sort(char[] arr1)
            {
                int max = arr1.Max();
                int min = arr1.Min();
                int range = max - min + 1;
                //Підрахунок входженя кожного елемента
                char[] count = new char[range];
                char[] output = new char[arr1.Length];
                //Відсортовуємо масив, спираючись на кількість входжень
                for (int i = 0; i < arr1.Length; i++)
                {
                    count[arr1[i] - min]++;
                }
                for (int i = 1; i < count.Length; i++)
                {
                    count[i] += count[i - 1];
                }
                for (int i = arr1.Length - 1; i >= 0; i--)
                {
                    output[count[arr1[i] - min] - 1] = arr1[i];
                    count[arr1[i] - min]--;
                }
                for (int i = 0; i < arr1.Length; i++)
                {
                    arr1[i] = output[i];
                }
            }
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            Stopwatch st;
            Random rand = new Random();
            HeapSort heapsort = new HeapSort();

            Console.WriteLine("Пірамідальне сортування(сортування кучею):");
            int[] arr = new int[10];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr = new int[100];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr = new int[500];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr = new int[1000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr = new int[2000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr = new int[5000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr = new int[10000];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = rand.Next(0, 101);
            st = Stopwatch.StartNew();
            heapsort.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            Console.WriteLine("\n");

            Console.WriteLine("Сортування Шелла:");
            ShellSort shellsort = new ShellSort();

            float[] array = new float[10];
            float max = 300;
            float min = 0;

            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds} ms");

            array = new float[100];
            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();

            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds} ms");

            array = new float[500];
            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds} ms");

            array = new float[1000];
            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            array = new float[2000];
            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            array = new float[5000];
            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            array = new float[10000];
            for (int i = 0; i < array.Length; i++)
                array[i] = (float)rand.NextDouble() * (max - min) + min;
            st = Stopwatch.StartNew();
            shellsort.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            Console.WriteLine("\n");

            Console.WriteLine("Сортування підрахунком:");
            CountingSort obje = new CountingSort();

            char[] arr1 = new char[10];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr1 = new char[100];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr1 = new char[500];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr1 = new char[1000];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr1 = new char[2000];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr1 = new char[5000];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds} ms");

            arr1 = new char[10000];
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = (char)rand.Next(-200, 11);
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            Console.ReadKey();
        }
    }

}
